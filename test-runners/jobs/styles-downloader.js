const fs        = require('fs');
const readline  = require('readline');
const AWS       = require('aws-sdk');
const config    = require('./styles-downloader-config.json');

const S3        = new AWS.S3();
const parser    = readline.createInterface({
    input: streamFromS3()
});

let streamStarted = false;
const fileStream  = fs.createWriteStream(config['updated-styles-location']);

parser.on('line', (str) => {
    if (!streamStarted) {
        fileStream.write('{"data": [0');
        streamStarted = true;
    }

    if (typeof str === 'string' && str.includes('": {')) {
        let tokens = str.split('"');
        fileStream.write(`,${Number(tokens[1])}`);
    }
});

parser.on('close', () => {
    fileStream.write(']}');
    fileStream.on('finish', () => {
        console.error(`Style IDs updated ==> ${config['updated-styles-location']}`);
    });
});

function streamFromS3() {
    return S3
        .getObject({ Bucket: config.bucket, Key: config.key })
        .createReadStream();
}