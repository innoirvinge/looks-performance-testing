'use strict';

// const styles = require('./data/SRE_STYLE_IDS.json').data;
const styles = require('./data/STYLE_IDS.json').data;
// const styles = require('./data/STYLE_IDS_SHORT.json').data;
// const styles = require('./data/STYLE_IDS_OLD.json').data;
const shoppers = require('./data/PREFERENCES.json').shoppers;

function pickRandom(array) {
	return array[Math.floor(Math.random() * array.length)];
}

function randomStyle() {
	return pickRandom(styles);
}

function randomShopper() {
	return pickRandom(shoppers);
}

module.exports = {
	looksService: function(context, events, done) {
		context.vars.styleID1 = randomStyle();
		context.vars.styleID2 = randomStyle();
		done();
	},
	shopperService: function(context, events, done) {
		context.vars.shopperId = randomShopper();
		done();
	},
	logStuff: function(requestParams, response, context, ee, next) {
		console.log(response.statusCode);
		next();
	}
}